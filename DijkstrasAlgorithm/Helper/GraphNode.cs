﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DijkstrasAlgorithm
{

    public class GraphNode
    {
        public string NodeName { get; }
        public int Distance { get; set; }
        public Dictionary<GraphNode, int> Neighbors { get; }
        public GraphNode Previous { get; set; }
        public bool isVisited { get; set; } = false;

        public GraphNode(string nodeName)
        {
            NodeName = nodeName;
            Distance = int.MaxValue;
            Neighbors = new Dictionary<GraphNode, int>();
        }

    }
}

