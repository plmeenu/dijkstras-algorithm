﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DijkstrasAlgorithm
{
    public class ShortestDataPath
    {
        public List<string> NodeNames { get; set; }
        public int Distance;
    }
}
