﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace DijkstrasAlgorithm.Helper
{
    public class Graph
    {
        public Dictionary<string, GraphNode> nodes;

        public Graph()
        {
            nodes = new Dictionary<string, GraphNode>();
        }

        public void AddNode(string id)
        {
            var node = new GraphNode(id);
            nodes[id] = node;
        }

        public void AddEdge(string from, string to, int cost, bool unidirectional = false)
        {
            if (!nodes.ContainsKey(from) || !nodes.ContainsKey(to))
            {
                throw new ArgumentException("Both nodes must exist in the graph.");
            }

            nodes[from].Neighbors[nodes[to]] = cost;
            if (!unidirectional)
                nodes[to].Neighbors[nodes[from]] = cost; // bidirectional edge
        }

        public ShortestDataPath GetShortestPath(string fromNode, string toNode, Dictionary<string, GraphNode> nodes)
        {
           try
            {
                if (nodes == null || !nodes.ContainsKey(fromNode) || !nodes.ContainsKey(toNode))
                {
                    throw new ArgumentException("Both nodes must exist in the graph.");
                }

                //set fromNode distance as zero
                nodes[fromNode].Distance = 0;

                foreach (var node in nodes)
                {
                    //current node is unvisited node with smallest distance
                    var currentNode = nodes.Where(x => !x.Value.isVisited).OrderBy(x => x.Value.Distance).First();

                    foreach (var neighbourNode in currentNode.Value.Neighbors)
                    {
                        var updateNode = nodes[neighbourNode.Key.NodeName];
                        if (!updateNode.isVisited)
                        {
                            var newDistance = neighbourNode.Value + currentNode.Value.Distance;
                            if (newDistance < updateNode.Distance)
                            {
                                updateNode.Distance = newDistance;
                                updateNode.Previous = currentNode.Value;
                            }
                        }

                    }

                    currentNode.Value.isVisited = true;
                }
                var shortestPathNode = nodes[toNode];

               return new ShortestDataPath()
                {
                    Distance = shortestPathNode.Distance,
                    NodeNames = BuildPath(shortestPathNode, fromNode, nodes)
                };
            }
            catch(Exception ex)
            {
                throw ex;
            }
           
          
        }

        private List<String> BuildPath(GraphNode node, string sourceNode, Dictionary<string, GraphNode> nodes)
        {
            var pathList = new List<string> { node.NodeName };
            while (node.Previous != null)
            {
                pathList.Add(node.Previous.NodeName);
                node = node.Previous;
            }
            pathList.Reverse();
            return pathList;
        }
    }
}
