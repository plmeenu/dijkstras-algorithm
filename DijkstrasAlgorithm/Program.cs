﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DijkstrasAlgorithm.Helper;

namespace DijkstrasAlgorithm
{

	public class Program
	{
		public static void Main(string[] args)
		{
			try
			{
				Graph graph = GetGraph();

				var fromNode = "A";
				var toNode = "D";
				var shortestPath = graph.GetShortestPath(fromNode, toNode, graph.nodes);
				Console.WriteLine(string.Format("From {0} to {1}, the shortest path is {2}, spanning a distance of {3}.", fromNode, toNode, string.Join(", ", shortestPath.NodeNames), shortestPath.Distance));
				Console.ReadKey();
			}
			catch (Exception ex)
			{
				Console.WriteLine("An error occurred: " + ex.Message);
			}
		}

		private static Graph GetGraph()
		{
			var graph = new Graph();
			graph.AddNode("A");
			graph.AddNode("B");
			graph.AddNode("C");
			graph.AddNode("D");
			graph.AddNode("E");
			graph.AddNode("F");
			graph.AddNode("G");
			graph.AddNode("H");
			graph.AddNode("I");

			graph.AddEdge("A", "B", 4);
			graph.AddEdge("A", "C", 6);
			graph.AddEdge("B", "F", 2);
			graph.AddEdge("C", "D", 8);
			graph.AddEdge("D", "E", 4);
			graph.AddEdge("D", "G", 1);
			graph.AddEdge("E", "B", 2, true);
			graph.AddEdge("E", "F", 3);
			graph.AddEdge("E", "I", 8);
			graph.AddEdge("F", "G", 4);
			graph.AddEdge("F", "H", 6);
			graph.AddEdge("G", "H", 5);
			graph.AddEdge("G", "I", 5);
			return graph;
		}
	}
}
