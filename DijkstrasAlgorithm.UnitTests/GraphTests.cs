﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DijkstrasAlgorithm;
using DijkstrasAlgorithm.Helper;
using System.Collections.Generic;

namespace DijkstrasAlgorithmUnitTest
{
    [TestClass]
    public class GraphTests
    {
        [TestMethod]
        public void TestGetShortestPath_ForValidNodes()
        {
            // Arrange
            Graph graph = GetGraph();
            var fromNode = "A";
            var toNode = "D";
            var expectedDistance = 11;
            var expectedNodeNameList = new List<string> { "A", "B", "F", "G", "D" };

            // Act
            var shortestPath = graph.GetShortestPath(fromNode, toNode, graph.nodes);

            // Assert
            Assert.IsNotNull(shortestPath);
            Assert.AreEqual(shortestPath.Distance, expectedDistance);
            CollectionAssert.AreEqual(shortestPath.NodeNames,expectedNodeNameList);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestGetShortestPath_ThrowsArgumentException_WhenNodesNotExist()
        {
            // Arrange
            Graph graph = GetGraph();
            var fromNode = "A";
            var toNode = "Z"; // 'Z' is not a valid node in the graph

            // Act
            var shortestPath = graph.GetShortestPath(fromNode, toNode, graph.nodes);

            // Assert
            // The test will pass if an ArgumentException is thrown
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestGetShortestPath_NullNodesDictionary()
        {
            // Arrange
            Graph graph = GetGraph();
            var fromNode = "A";
            var toNode = "B";

            // Act
            var shortestPath = graph.GetShortestPath(fromNode, toNode, null);

            // Assert
            // The test passes if an ArgumentNullException is thrown
        }
        private Graph GetGraph()
        {
            var graph = new Graph();
            graph.AddNode("A");
            graph.AddNode("B");
            graph.AddNode("C");
            graph.AddNode("D");
            graph.AddNode("E");
            graph.AddNode("F");
            graph.AddNode("G");
            graph.AddNode("H");
            graph.AddNode("I");

            graph.AddEdge("A", "B", 4);
            graph.AddEdge("A", "C", 6);
            graph.AddEdge("B", "F", 2);
            graph.AddEdge("C", "D", 8);
            graph.AddEdge("D", "E", 4);
            graph.AddEdge("D", "G", 1);
            graph.AddEdge("E", "B", 2, true);
            graph.AddEdge("E", "F", 3);
            graph.AddEdge("E", "I", 8);
            graph.AddEdge("F", "G", 4);
            graph.AddEdge("F", "H", 6);
            graph.AddEdge("G", "H", 5);
            graph.AddEdge("G", "I", 5);
            return graph;
        }
    }
}
