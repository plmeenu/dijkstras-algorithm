# Project Name

## Introduction
This project is built on .NET 8 framework and requires .NET 8 to be compiled and executed successfully.

## Prerequisites
- .NET 8 SDK: Ensure that you have .NET 8 SDK installed on your machine before compiling this project.

## Compilation Instructions
To compile the project, follow these steps:

1. Clone the repository to your local machine.
2. Open a terminal or command prompt.
3. Navigate to the root directory of the project.
4. Run the following command to compile the project:
    ```
    dotnet build
    ```
5. If the compilation is successful, you can then run the executable file generated in the `bin` directory.
